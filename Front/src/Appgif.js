import React from 'react';
import './App.css';

import { Link, Route } from "wouter";
import Gifs from "./componentes/Gifs";
import Datos1 from "./componentes/Datos1";
import Datos2 from "./componentes/Datos2";
import Chart from "./componentes/Grafica1";
import Chart2 from "./componentes/Grafica2";


function App() {

    return (
        <div className="App">
            <header className="App-header">
                <h1>Conferencia</h1>

                <Link to='/gif/gatos' className="links">Hola</Link>

                <Route
                    component={Gifs}
                    path="/gif/:keyword"/>

                {/*Gifs keyword='drift'/>*/}
            </header>
        </div>
    );
}

export default App;
